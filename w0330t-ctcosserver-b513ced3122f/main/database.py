import pymongo
from bson.json_util import *

class database():
    def __init__(self):
        """
        数据库初始化
        连接数据库
        """
        self.client = pymongo.MongoClient("mongodb://ctcos:ctcos1234ASDFqa@my.blueness.net:27017/ctcos")
        self.db = self.client.ctcos

    def get_collection(self, collection_name, query = {}, projection = None):
        """
        获取集合的基本用法
        返回一个列表……以后可能会填充
        """
        collection = self.db[collection_name]
        result = collection.find(query, projection)
        # return list(result) #把结果转换成列表，好像在这里没啥必要了
        return dumps(result)

    def get_one_document(self,collection_name, query = {}, projection = None ):
        """
        获取一个document
        :param collection_name: 集合
        :param query: 过滤条件
        :param projection:
        :return: json的结果
        """
        collection = self.db[collection_name]
        result = collection.find_one(query, projection)
        # return list(result) #把结果转换成列表，好像在这里没啥必要了
        return dumps(result)

    def update_collection(self, collection_name, query, update):
        """
        更新
        :param collection_name: 集合名字
        :param query: 更新条件
        :param update: 更新数据
        :return:
        """
        collection = self.db[collection_name]
        collection.update_one(query, {"$set" : update})
    def delete_collection(self, collection_name, query):
        """
        更新
        :param collection_name: 集合名字
        :param query: 删除条件
        :return:
        """
        collection = self.db[collection_name]
        collection.remove(query)
    def insert_collection(self, collection_name, query):
        """
        更新
        :param collection_name: 集合名字
        :param query: 插入条件
        :return:
        """
        collection = self.db[collection_name]
        collection.insert(query)
