﻿from flask import request,Flask,render_template 
import ast
from main.database import *
from flask import Flask, render_template  
from flask.ext.script import Manager  
from flask.ext.bootstrap import Bootstrap  
from flask.ext.wtf import Form  
from wtforms import StringField, SubmitField  
from wtforms.validators import Required
import os
from werkzeug.utils import secure_filename
from main.database import *

# ALLOWED_EXTENSIONS = set(['pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
bootstrap=Bootstrap(app)
app.config['SECRET_KEY'] = 'hard to guess string'  
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['UPLOAD_FOLDER'] = '/usr/src/CTCOSServer/static/upload/images'

class NameForm(Form):  
    username = StringField('用户名', validators=[Required()])  
    password = StringField('密码', validators=[Required()])  
    submit = SubmitField('Submit')  
    
@app.route('/')
def Index():
    name = None  
    form = NameForm() 
    if form.validate_on_submit():  
        username = form.username.data  
        password = form.password.data  
        return 'success'
    else:
        return render_template('admin/login.html',form=form)
@app.route('/upload_file/<type>', methods=['GET', 'POST'])
def file(type):
    if type == 'image' and request.method == 'POST':
        file = request.files.get('file')
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return 'upload success'
    return ''

@app.route('/option')
def Option():
    return '''<form action="/db" method="post">
              <select name="manufacturer" id="manufacturer">
              <option></option>
              <option selected="selected">--请选择更改方式--</option>
              <option >查找</option>
              <option >删除</option>
              <option >增加</option>
              <option >修改</option>
              </select>
              <p><input name="id"></p>
               <p><input name="dbtype"></p>
              <p><button type="submit">提交</button></p>
              </form>'''
@app.route('/db',  methods=['GET', 'POST'])
def Db():
    collection = "log"
    db = database()
    choose=request.values.get("manufacturer")
    if choose == "查找":
        if(request.values.get('id')):
            id = int(request.values.get('id'))
            title=str(request.values.get('dbtype'))
            print(title)
            result = db.get_one_document(collection, {title: id})
            title=result.values.get('')
        else:
            result = db.get_collection(collection)
        return result
    if choose == "修改":
        data = request.get_data()
        data = data.decode()
        data = ast.literal_eval(data)
        id = int(request.values.get('_id'))
        result = db.update_collection(collection, {'_id': id}, data)
        return data
    if choose == "删除":
        id = int(request.values.get('id'))
        p = "_id"
        result = db.delete_collection(collection, {'_id': id})
        return ""
    if choose == "增加":
        id = int(request.values.get('id'))
        p = "_id"
        result = db.delete_collection(collection, {'_id': id})
        return ""
     
@app.route('/Insert',  methods=['GET', 'POST'])
def Insert():
    return '''<form action="/" method="post">
              <p>货物单号<input name="id"></p>
              <p>公司名称<input name="Com"></p>
              <p>货物名称<input name="goods"></p>
              <p>数量<input name="number"></p>
              <p><button type="submit">提交</button></p>
              </form>'''
@app.route('/login',  methods=['GET', 'POST'])
def Login():
    username=str(request.values.get('username'))
    password=str(request.values.get('password'))
    db = database()
    result = db.get_collection('users', {"userName":username,"password":password})
    if result.strip()!='[]':
        return 'success'
    else:
        return 'fail'

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')